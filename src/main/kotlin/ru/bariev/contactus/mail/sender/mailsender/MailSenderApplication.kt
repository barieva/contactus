package ru.bariev.contactus.mail.sender.mailsender

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.context.event.ApplicationReadyEvent
import org.springframework.boot.runApplication
import org.springframework.context.event.EventListener
import ru.bariev.contactus.mail.sender.mailsender.service.EmailSenderService

@SpringBootApplication
class MailSenderApplication(@Autowired val service: EmailSenderService) {


    @EventListener(ApplicationReadyEvent::class)
    fun triggerMail() {
        service.sendSimpleEmail("receiver@gmail.com","Hello friend how are you today? I`ve created app, which goes through loop 10 times and sends you an email.<br> Thank you! ","Important")
    }
}
fun main(args: Array<String>) {
    runApplication<MailSenderApplication>(*args)
}