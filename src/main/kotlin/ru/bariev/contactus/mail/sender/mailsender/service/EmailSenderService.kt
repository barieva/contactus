package ru.bariev.contactus.mail.sender.mailsender.service

import kotlin.math.log
import lombok.extern.slf4j.Slf4j
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.mail.SimpleMailMessage
import org.springframework.mail.javamail.JavaMailSender
import org.springframework.stereotype.Service

@Service
@Slf4j
class EmailSenderService(@Autowired val mailSender:JavaMailSender) {



    fun sendSimpleEmail(toEmail: String, body: String, subject: String) {

        for (i in 1..10) {
            val message = SimpleMailMessage()
            message.setFrom("sender@gmail.com")
            message.setTo(toEmail)
            message.setText(body)
            message.setSubject(subject)
            mailSender.send(message)
            println("Main`ve sent")
        }
    }
}